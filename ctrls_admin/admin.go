package ctrls_admin

import (
	"cb-board/confs"
	"cb-board/models"
	"cblib"
	"errors"
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"github.com/satori/go.uuid"

	mgo "gopkg.in/mgo.v2"
)

type adminCtrls struct {
	db *mgo.Database
}

func NewAdminCtrls(db *mgo.Database) *adminCtrls {
	rc := new(adminCtrls)
	rc.db = db
	return rc
}

func (ac *adminCtrls) GetLogin(username string) (*models.AdminLoginJson, int, error) {
	_, ok := confs.Conf.AdminPublicKey[username]
	if !ok {
		return nil, http.StatusNotFound, errors.New("the username is not found")
	}
	al := models.AdminLogin{}
	al.Username = username
	al.Text = uuid.NewV4().String()
	err := al.Insert(ac.db)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	aj := al.Json()
	return &aj, http.StatusOK, nil
}

type PostLoginOpts struct {
	LoginID   string
	Signature string
}

func (ac *adminCtrls) PostLogin(opts PostLoginOpts) (int, error) {
	if !bson.IsObjectIdHex(opts.LoginID) {
		return http.StatusUnprocessableEntity, errors.New("the login's ID is not correct")
	}
	al, err := models.GetAdminLogin(ac.db, bson.ObjectIdHex(opts.LoginID))
	if err != nil {
		return http.StatusNotFound, errors.New("the login's ID is not found")
	}
	if len(al.Signature) > 0 {
		return http.StatusUnprocessableEntity, errors.New("this login's ID is already signed")
	}
	pubk := confs.Conf.AdminPublicKey[al.Username]
	is, _ := cblib.Verify([]byte(pubk), []byte(al.Text), []byte(opts.Signature))
	if !is {
		return http.StatusUnauthorized, errors.New("signature is not correct")
	}
	al.Signature = opts.Signature
	al.Update(ac.db)
	return http.StatusNoContent, nil
}
