package ctrls_admin

import (
	"cb-board/confs"
	"cb-board/utils"
	"cblib"
	"crypto/rand"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	mgo "gopkg.in/mgo.v2"
)

func startDB() *mgo.Database {
	ses, err := mgo.Dial("localhost")
	if err != nil {
		utils.Fatal("cannot dial mongo", err)
	}
	db := ses.DB("test")
	return db
}

func endDB(db *mgo.Database) {
	err := db.DropDatabase()
	if err != nil {
		utils.Fatal("cannot delete db", err)
	}
}

func NewAdminTest(t *testing.T) *cblib.Keys {
	k, err := cblib.GenerateKeys(cblib.P256, rand.Reader)
	assert.Nil(t, err)
	confs.Conf.AdminPublicKey = map[string]string{"admin": string(k.PublicKey)}
	return k
}

func TestLoginSuccess(t *testing.T) {
	db := startDB()
	defer endDB(db)
	k := NewAdminTest(t)
	ac := NewAdminCtrls(db)
	login, status, err := ac.GetLogin("admin")
	assert.Equal(t, http.StatusOK, status)
	assert.Nil(t, err)
	assert.NotNil(t, login)
	lopts := PostLoginOpts{}
	lopts.LoginID = login.ID
	sig, err := cblib.Sign([]byte(login.Text), k.PrivateKey, rand.Reader)
	assert.Nil(t, err)
	lopts.Signature = string(sig)
	status, err = ac.PostLogin(lopts)
	assert.Equal(t, http.StatusNoContent, status)
	assert.Nil(t, err)
}
