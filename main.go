package main

import (
	"cb-board/api"
	"cb-board/utils"
	"cb-board/ws"
	"cb-board/wschannels"
	"flag"

	mgo "gopkg.in/mgo.v2"
)

func main() {
	ses, err := mgo.Dial("localhost")
	if err != nil {
		utils.Fatal("cannot dial mongo", err)
	}
	str := flag.String("db", "cb-board", "database's name")
	flag.Parse()
	db := ses.DB(*str)
	go ws.HubRun(wschannels.Routes)

	router := api.NewRouter(db)
	router.Engine().Run() // listen and serve on 0.0.0.0:8080
}
