package api

import (
	"cb-board/ctrls"
	"cb-board/utils"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

func (rt *router) GetUsers(c *gin.Context) {
	perPage := utils.PageSize(c.Request)
	uc := ctrls.NewUserCtrls(rt.db)
	users, status, _ := uc.GetUsers(&perPage)
	c.JSON(status, users)
}

func (rt *router) GetUser(c *gin.Context) {
	id := c.Param("id")
	uc := ctrls.NewUserCtrls(rt.db)
	user, status, err := uc.GetUser(id)
	if err != nil {
		JsonError(c, status, err)
		return
	}
	c.JSON(status, user)
}

func (rt *router) GetUserBySha1(c *gin.Context) {
	sha1 := c.Param("sha1")
	uc := ctrls.NewUserCtrls(rt.db)
	user, status, err := uc.GetUserBySha1(sha1)
	if err != nil {
		JsonError(c, status, err)
		return
	}
	c.JSON(status, user)
}

func (rt *router) DeleteUser(c *gin.Context) {
	opts := ctrls.DeleteUserOpts{}
	err := c.ShouldBindWith(&opts, binding.JSON)
	if err != nil {
		JsonError(c, http.StatusInternalServerError, err)
		return
	}
	uc := ctrls.NewUserCtrls(rt.db)
	status, err := uc.DeleteUser(&opts)
	if err != nil {
		JsonError(c, status, err)
		return
	}
	c.Status(status)
}

func (rt *router) Register(c *gin.Context) {
	opts := ctrls.RegisterOpts{}
	err := c.ShouldBindWith(&opts, binding.JSON)
	if err != nil {
		JsonError(c, http.StatusInternalServerError, err)
		return
	}
	rc := ctrls.NewRegisterCtrls(rt.db)
	uj, status, err := rc.RegisterKey(&opts)
	if err != nil {
		JsonError(c, status, err)
		return
	}
	c.JSON(status, uj)
}
