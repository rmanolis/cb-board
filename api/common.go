package api

import "github.com/gin-gonic/gin"

func JsonError(c *gin.Context, status int, err error) {
	c.JSON(status, map[string]interface{}{"error": err.Error()})
}
