package api

import (
	"cb-board/ctrls"
	"cb-board/utils"
	"net/http"

	"github.com/gin-gonic/gin/binding"

	"github.com/gin-gonic/gin"
)

func (rt *router) GetPosts(c *gin.Context) {
	perPage := utils.PageSize(c.Request)
	pc := ctrls.NewPostCtrls(rt.db)
	posts, status, _ := pc.GetPosts(&perPage)
	c.JSON(status, posts)
}

func (rt *router) GetPost(c *gin.Context) {
	id := c.Param("id")
	pc := ctrls.NewPostCtrls(rt.db)
	pj, status, err := pc.GetPostByID(id)
	if err != nil {
		JsonError(c, status, err)
		return
	}
	c.JSON(status, pj)
}

func (rt *router) GetPostsByUser(c *gin.Context) {
	perPage := utils.PageSize(c.Request)
	id := c.Param("id")
	pc := ctrls.NewPostCtrls(rt.db)
	pjs, status, err := pc.GetPostsByUser(&perPage, id)
	if err != nil {
		JsonError(c, status, err)
		return
	}
	c.JSON(status, pjs)
}

func (rt *router) CreatePost(c *gin.Context) {
	opts := ctrls.CreatePostOpts{}
	err := c.ShouldBindWith(&opts, binding.JSON)
	if err != nil {
		JsonError(c, http.StatusInternalServerError, err)
		return
	}
	pc := ctrls.NewPostCtrls(rt.db)
	pj, status, err := pc.CreatePost(&opts)
	if err != nil {
		JsonError(c, status, err)
		return
	}
	c.JSON(status, pj)
}

func (rt *router) DeletePost(c *gin.Context) {
	opts := ctrls.DeletePostOpts{}
	err := c.ShouldBindWith(&opts, binding.JSON)
	if err != nil {
		JsonError(c, http.StatusInternalServerError, err)
		return
	}
	pc := ctrls.NewPostCtrls(rt.db)
	status, err := pc.DeletePost(&opts)
	if err != nil {
		JsonError(c, status, err)
		return
	}
	c.Status(status)
}
