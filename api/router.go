package api

import (
	"cb-board/ws"

	"github.com/gin-gonic/gin"
	mgo "gopkg.in/mgo.v2"
)

type router struct {
	db     *mgo.Database
	engine *gin.Engine
}

func NewRouter(db *mgo.Database) *router {
	rt := new(router)
	rt.db = db
	e := gin.Default()

	e.GET("/api/v1/users", rt.GetUsers)
	e.GET("/api/v1/search/users/sha1/:sha1", rt.GetUserBySha1)
	e.GET("/api/v1/users/:id", rt.GetUser)
	e.GET("/api/v1/users/:id/posts", rt.GetPostsByUser)
	e.POST("/api/v1/users", rt.Register)
	e.PUT("/api/v1/users/delete", rt.DeleteUser)
	e.GET("/api/v1/posts", rt.GetPosts)
	e.GET("/api/v1/posts/:id", rt.GetPost)
	e.POST("/api/v1/posts", rt.CreatePost)
	e.PUT("/api/v1/posts/delete", rt.DeletePost)

	e.GET("/ws", func(c *gin.Context) {
		ws.ServeWs(c.Writer, c.Request)
	})
	e.Static("/static", "./static")
	e.StaticFile("/", "./static/index.html")

	rt.engine = e
	return rt
}

func (rt *router) Engine() *gin.Engine {
	return rt.engine
}
