package wschannels

import (
	"cb-board/client/actions"
	"cb-board/client/stedux"
	"encoding/json"

	"github.com/gopherjs/websocket"

	"honnef.co/go/js/dom"
)

type WSMessage struct {
	Channel string `json:"channel"`
	Message string `json:"message"`
}

func Router() error {
	host := dom.GetWindow().Location().Host
	conn, err := websocket.Dial("ws://" + host + "/ws") // Blocks until connection is established.
	if err != nil {
		// handle error
		println(err.Error())
		return err
	}

	for {
		buf := make([]byte, 1024)
		n, err := conn.Read(buf[:])
		if err != nil {
			println(err.Error())
			return err
		}
		wsmsg := WSMessage{}
		err = json.Unmarshal(buf[:n], &wsmsg)
		if err != nil {
			println(err.Error())
			return err
		}
		println(wsmsg.Channel, wsmsg.Message)
		switch wsmsg.Channel {
		case "deleted-post":
			stedux.Dispatch(&actions.ActionPostDeleted{ID: wsmsg.Message})
		case "new-post":
			stedux.Dispatch(&actions.ActionNewPostReceived{ID: wsmsg.Message})
		}
	}
}
