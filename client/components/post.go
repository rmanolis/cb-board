package components

import (
	"cb-board/client/actions"
	"cb-board/client/services"
	"cb-board/client/stedux"

	"github.com/PalmStoneGames/polymer"
)

// cb-component-post
type PostComponent struct {
	*polymer.Proto
	Post           services.PostJson   `polymer:"bind"`
	ShowDeletePost bool                `polymer:"bind"`
	ShowError      bool                `polymer:"bind"`
	Error          string              `polymer:"bind"`
	DeletePost     chan *polymer.Event `polymer:"handler"`
}

func (t *PostComponent) Ready() {
	go func() {
		for {
			select {
			case _ = <-t.DeletePost:
				println("delete post with id ", t.Post.ID)
				t.ShowDeletePost = true
				t.Notify("showDeletePost")
			}
		}
	}()
}

func (t *PostComponent) Created() {
	stedux.Register(t.actions)
}

func (t *PostComponent) actions(action interface{}) {
	switch ac := action.(type) {
	case *actions.ActionPostDeleted:
		if ac.HasError {
			println("post compoment received error from deleting id " + ac.ID)
			t.ShowError = true
			t.Error = ac.Error
			t.Notify("showError", "error")
		} else {
			t.ShowDeletePost = false
			t.Notify("showDeletePost")
		}
	}
	stedux.Listeners.Fire()
}
