package components

import "github.com/PalmStoneGames/polymer"

import "github.com/gopherjs/gopherjs/js"

// cb-component-textarea
type Textarea struct {
	*polymer.Proto
	Value string              `polymer:"bind"`
	Copy  chan *polymer.Event `polymer:"handler"`
}

func (t *Textarea) Ready() {
	println()
	go func() {
		for {
			select {
			case _ = <-t.Copy:
				t.This().Get("shadowRoot").Call("querySelector", "textarea").Call("select")
				js.Global.Get("document").Call("execCommand", "Copy")
			}
		}
	}()
}
