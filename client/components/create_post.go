package components

import (
	"cb-board/client/services"
	"cb-board/client/utils"

	"github.com/PalmStoneGames/polymer"
)

// cb-component-create-post
type CreatePostComponent struct {
	*polymer.Proto
	Text       string              `polymer:"bind"`
	Signature  string              `polymer:"bind"`
	PublicKey  string              `polymer:"bind"`
	Error      string              `polymer:"bind"`
	ShowError  bool                `polymer:"bind"`
	CreatePost chan *polymer.Event `polymer:"handler"`
}

func (t *CreatePostComponent) Ready() {
	go func() {
		for {
			select {
			case _ = <-t.CreatePost:
				opts := services.CreatePostOpts{}
				opts.Text = t.Text
				opts.Signature = t.Signature
				opts.Sha1, _ = utils.PublicKeyBodyToSha1(t.PublicKey)
				services.CreatePost(&opts, func(ej *services.ErrorJson, status int, pj *services.PostJson) {
					if ej != nil {
						t.Error = ej.Error
						t.ShowError = true
						t.Notify("error", "showError")
						return
					}
					t.PublicKey = ""
					t.Text = ""
					t.Signature = ""
					t.Notify("publicKey", "text", "signature")
				})
			}
		}
	}()
}
