package components

import (
	"cb-board/client/actions"
	"cb-board/client/services"
	"cb-board/client/stedux"
	"cb-board/client/utils"

	"github.com/PalmStoneGames/polymer"
)

// cb-component-delete-post
type DeletePostComponent struct {
	*polymer.Proto
	ID           string              `polymer:"bind"`
	Text         string              `polymer:"bind"`
	RandomString string              `polymer:"bind"`
	Signature    string              `polymer:"bind"`
	DeletePost   chan *polymer.Event `polymer:"handler"`
}

func (t *DeletePostComponent) Ready() {
	services.GetPost(t.ID, func(ej *services.ErrorJson, status int, pj *services.PostJson) {
		if ej != nil {
			stedux.Dispatch(&actions.ActionPostDeleted{ID: t.ID, Error: ej.Error, HasError: true})
			return
		}
		t.RandomString = utils.RandomString(20)
		t.Text = pj.ID + t.RandomString
		t.Notify("randomString", "text")
	})
	go func() {
		for {
			select {
			case _ = <-t.DeletePost:
				opts := services.DeletePostOpts{}
				opts.RandomString = t.RandomString
				opts.Signature = t.Signature
				opts.ID = t.ID
				services.DeletePost(&opts, func(ej *services.ErrorJson, status int) {
					if ej != nil {
						stedux.Dispatch(&actions.ActionPostDeleted{ID: t.ID, Error: ej.Error, HasError: true})
						return
					}
					stedux.Dispatch(&actions.ActionPostDeleted{ID: t.ID, HasError: false})
					t.Text = ""
					t.Signature = ""
					t.RandomString = ""
					t.Notify("randomString", "text", "signature")
				})
			}
		}
	}()
}
