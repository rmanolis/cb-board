package tests

import (
	"cb-board/client/services"
	"flag"

	"github.com/gopherjs/gopherjs/js"
)

func changeServerLink() {
	link := flag.String("server", "http://localhost:8080", "the http link of the server")
	flag.Parse()
	services.Server = *link
}

func putXMLHttpRequest() {
	obj := js.Global.Call("require", "xmlhttprequest")
	js.Global.Set("XMLHttpRequest", obj.Get("XMLHttpRequest"))
	println(js.Global.Get("XMLHttpRequest"))
}
