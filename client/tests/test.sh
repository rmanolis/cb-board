#!/bin/sh
# For this test to run from node.js we need to install two packages from npm
# first we download the source-map-support so we can run the tests from node.js
# npm install source-map-support
# secondly we download the xmlhttprequest, so we can run XMLHttpRequest from the node.js
# npm install xmlhttprequest
# Also follow the instruction from this link to remove this warning when using cblib
# "warning: system calls not available, see https://github.com/gopherjs/gopherjs/blob/master/doc/syscalls.md"


gopherjs test -v cb-board/client/tests 
