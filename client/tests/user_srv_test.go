package tests

import (
	"cb-board/client/services"
	"testing"
	"time"
)

func TestCreateDeleteUser(t *testing.T) {
	putXMLHttpRequest()
	changeServerLink()
	t.Log("starting creaing")
	copts := services.CreateUserOpts{
		PublicKey: `-----BEGIN CB PUBLIC KEY-----
curve: P256

MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEpYIZWQlClcP4uREiXaHG5W+hQUvv
DqAYTQWVHmzPEprDR/3cAbTUaytKGNA0POHuS6NsPwYsQ+Q8Ydk7lJ3hNQ==
-----END CB PUBLIC KEY-----`,
	}
	services.CreateUser(&copts, func(err *services.ErrorJson, status int, uj *services.UserJson) {
		t.Log("created", status, uj == nil)
		if err == nil {
			dopts := services.DeleteUserOpts{
				UserID:       uj.ID,
				PublicKey:    uj.PublicKey,
				RandomString: "1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",
				Signature: `-----BEGIN CB SIGNATURE-----
Jf+BAwEBCVNpZ25hdHVyZQH/ggABAgEBUgH/hAABAVMB/4QAAAAK/4MFAQL/hgAA
AEn/ggEhAgGTM4J0JLe+4PxRkIcpnOrqDdW7O02l3IKzf0KoA4yxASECTicFRpkl
hfqQCeXGp9xSybWrJgH1btIg4RMkoGg7TeEA
-----END CB SIGNATURE-----`,
			}
			services.DeleteUser(&dopts, func(err *services.ErrorJson, status int) {
				t.Log("deleted ", status)
				if err != nil {
					t.Error(err.Error)
				}
			})
		} else {
			t.Error(err.Error)
		}
	})
	time.Sleep(10 * time.Second)

}
