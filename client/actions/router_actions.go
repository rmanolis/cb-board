package actions

type ActionChangePath struct {
	Path string
	Page string
	ID   string
}
