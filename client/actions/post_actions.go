package actions

type ActionPostDeleted struct {
	ID       string
	Error    string
	HasError bool
}

type ActionNewPostReceived struct {
	ID string
}
