package main

import (
	"cb-board/client/components"
	"cb-board/client/pages"
	"cb-board/client/wschannels"

	"github.com/PalmStoneGames/polymer"

	"honnef.co/go/js/dom"
)

func init() {
	polymer.Register("cb-page-registration", &pages.Registration{})
	polymer.Register("cb-page-router", &pages.Router{})
	polymer.Register("cb-page-profile", &pages.Profile{})
	polymer.Register("cb-page-profiles", &pages.Profiles{})
	polymer.Register("cb-page-posts", &pages.Posts{})
	polymer.Register("cb-page-post", &pages.Post{})

	polymer.Register("cb-component-create-post", &components.CreatePostComponent{})
	polymer.Register("cb-component-delete-post", &components.DeletePostComponent{})
	polymer.Register("cb-component-post", &components.PostComponent{})
	polymer.Register("cb-component-textarea", &components.Textarea{})
}

func main() {
	println(dom.GetWindow().Location().URLUtils.Pathname)
	if dom.GetWindow().Location().Hash == "" {
		dom.GetWindow().Location().Hash = "#/"
		dom.GetWindow().Location().Call("reload", nil)
	}
	wschannels.Router()
	println("hello world?!")
}
