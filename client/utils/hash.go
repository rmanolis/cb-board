package utils

import (
	"crypto/sha1"
	"encoding/hex"
	"io"
	"math/rand"
	"strings"
	"time"
)

func between(value string, a string, b string) string {
	// Get substring between two strings.
	posFirst := strings.Index(value, a)
	if posFirst == -1 {
		return ""
	}
	posLast := strings.Index(value, b)
	if posLast == -1 {
		return ""
	}
	posFirstAdjusted := posFirst + len(a)
	if posFirstAdjusted >= posLast {
		return ""
	}
	return value[posFirstAdjusted:posLast]
}

func PublicKeyBodyToSha1(pk string) (string, error) {
	middle := between(pk, "-----BEGIN CB PUBLIC KEY-----", "-----END CB PUBLIC KEY-----")
	h := sha1.New()
	_, err := io.WriteString(h, middle)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(h.Sum(nil)), nil
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789")

func RandomString(n int) string {
	rand.Seed(time.Now().Unix())
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
