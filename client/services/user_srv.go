package services

import (
	"encoding/json"
	"time"

	"honnef.co/go/js/xhr"
)

type UserJson struct {
	ID        string    `json:"id"`
	PublicKey string    `json:"public_key"`
	Sha1      string    `json:"sha1"`
	CreatedAt time.Time `json:"created_at"`
}

func serializeUser(text string) (*UserJson, *ErrorJson) {
	uj := new(UserJson)
	err := json.Unmarshal([]byte(text), &uj)
	if err != nil {
		return nil, &ErrorJson{Error: err.Error()}
	}
	return uj, nil
}

func serializeUsers(text string) ([]UserJson, *ErrorJson) {
	ujs := []UserJson{}
	err := json.Unmarshal([]byte(text), &ujs)
	if err != nil {
		return nil, &ErrorJson{Error: err.Error()}
	}
	return ujs, nil
}

type CreateUserOpts struct {
	PublicKey string `json:"public_key"`
}

func CreateUser(opts *CreateUserOpts, outfn func(*ErrorJson, int, *UserJson)) {
	go func() {
		req := xhr.NewRequest("POST", Server+"/api/v1/users")
		b, err := json.Marshal(opts)
		if err != nil {
			outfn(getError(err.Error()), 0, nil)
			return
		}
		err = req.Send(string(b))
		if err != nil {
			outfn(getError(err.Error()), 0, nil)
			return
		}
		if req.Status >= BAD_REQUEST {
			ej := getError(req.ResponseText)
			outfn(ej, req.Status, nil)
		} else {
			uj, ej := serializeUser(req.ResponseText)
			outfn(ej, req.Status, uj)
		}
	}()
}

type DeleteUserOpts struct {
	UserID       string `json:"user_id"`
	PublicKey    string `json:"public_key"`
	RandomString string `json:"random_string"`
	Signature    string `json:"signature"`
}

func DeleteUser(opts *DeleteUserOpts, outfn func(*ErrorJson, int)) {
	go func() {
		req := xhr.NewRequest("PUT", Server+"/api/v1/users/delete")
		b, err := json.Marshal(opts)
		if err != nil {
			outfn(getError(err.Error()), 0)
			return
		}
		err = req.Send(string(b))
		if err != nil {
			outfn(getError(err.Error()), 0)
			return
		}
		if req.Status >= BAD_REQUEST {
			ej := getError(req.ResponseText)
			outfn(ej, req.Status)
		} else {
			outfn(nil, req.Status)
		}
	}()
}

func GetUser(id string, outfn func(*ErrorJson, int, *UserJson)) {
	go func() {
		req := xhr.NewRequest("GET", Server+"/api/v1/users/"+id)
		err := req.Send(nil)
		if err != nil {
			outfn(getError(err.Error()), 0, nil)
			return
		}
		if req.Status >= BAD_REQUEST {
			ej := getError(req.ResponseText)
			outfn(ej, req.Status, nil)
		} else {
			uj, ej := serializeUser(req.ResponseText)
			outfn(ej, req.Status, uj)
		}
	}()
}

func GetUserBySha1(sha1 string, outfn func(*ErrorJson, int, *UserJson)) {
	go func() {
		req := xhr.NewRequest("GET", Server+"/api/v1/search/users/sha1/"+sha1)
		err := req.Send(nil)
		if err != nil {
			outfn(getError(err.Error()), 0, nil)
			return
		}
		if req.Status >= BAD_REQUEST {
			ej := getError(req.ResponseText)
			outfn(ej, req.Status, nil)
		} else {
			uj, ej := serializeUser(req.ResponseText)
			outfn(ej, req.Status, uj)
		}
	}()
}

func GetUsers(pp *PerPage, outfn func(*ErrorJson, int, []UserJson)) {
	go func() {
		var req *xhr.Request
		if pp != nil {
			req = xhr.NewRequest("GET", Server+"/api/v1/users?"+pp.Params())
		} else {
			req = xhr.NewRequest("GET", Server+"/api/v1/users")
		}
		err := req.Send(nil)
		if err != nil {
			outfn(getError(err.Error()), 0, nil)
			return
		}
		if req.Status >= BAD_REQUEST {
			ej := getError(req.ResponseText)
			outfn(ej, req.Status, nil)
		} else {
			ujs, ej := serializeUsers(req.ResponseText)
			outfn(ej, req.Status, ujs)
		}
	}()
}
