package services

import (
	"encoding/json"
	"strconv"
)

type PerPage struct {
	Page int `json:"page"`
	Size int `json:"size"`
}

func (pp *PerPage) Params() string {
	return "page=" + strconv.Itoa(pp.Page) + "&size=" + strconv.Itoa(pp.Size)
}

const BAD_REQUEST = 400

type ErrorJson struct {
	Error string `json:"error"`
}

var Server = ""

func getError(text string) *ErrorJson {
	ej := new(ErrorJson)
	ej.Error = text
	err := json.Unmarshal([]byte(text), &ej)
	if err != nil {
		return &ErrorJson{Error: err.Error()}
	}
	return ej
}
