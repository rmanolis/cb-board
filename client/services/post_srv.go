package services

import (
	"encoding/json"
	"time"

	"honnef.co/go/js/xhr"
)

type PostJson struct {
	ID        string    `json:"id"`
	Text      string    `json:"text"`
	Signature string    `json:"signature"`
	Sha1      string    `json:"sha1"`
	UserID    string    `json:"user_id"`
	CreatedAt time.Time `json:"created_at"`
}

type CreatePostOpts struct {
	Text      string `json:"text"`
	Signature string `json:"signature"`
	Sha1      string `json:"sha1"`
}

func serializePost(text string) (*PostJson, *ErrorJson) {
	uj := new(PostJson)
	err := json.Unmarshal([]byte(text), &uj)
	if err != nil {
		return nil, &ErrorJson{Error: err.Error()}
	}
	return uj, nil
}

func serializePosts(text string) ([]PostJson, *ErrorJson) {
	ujs := []PostJson{}
	err := json.Unmarshal([]byte(text), &ujs)
	if err != nil {
		return nil, &ErrorJson{Error: err.Error()}
	}
	return ujs, nil
}

func CreatePost(opts *CreatePostOpts, outfn func(*ErrorJson, int, *PostJson)) {
	go func() {
		req := xhr.NewRequest("POST", Server+"/api/v1/posts")
		b, err := json.Marshal(opts)
		if err != nil {
			outfn(getError(err.Error()), 0, nil)
			return
		}
		err = req.Send(string(b))
		if err != nil {
			outfn(getError(err.Error()), 0, nil)
			return
		}
		if req.Status >= BAD_REQUEST {
			ej := getError(req.ResponseText)
			outfn(ej, req.Status, nil)
		} else {
			pj, ej := serializePost(req.ResponseText)
			outfn(ej, req.Status, pj)
		}
	}()
}

type DeletePostOpts struct {
	ID           string `json:"id"`
	RandomString string `json:"random_string"`
	Signature    string `json:"signature"`
}

func DeletePost(opts *DeletePostOpts, outfn func(*ErrorJson, int)) {
	go func() {
		req := xhr.NewRequest("PUT", Server+"/api/v1/posts/delete")
		b, err := json.Marshal(opts)
		if err != nil {
			outfn(getError(err.Error()), 0)
			return
		}
		err = req.Send(string(b))
		if err != nil {
			outfn(getError(err.Error()), 0)
			return
		}
		if req.Status >= BAD_REQUEST {
			ej := getError(req.ResponseText)
			outfn(ej, req.Status)
		} else {
			outfn(nil, req.Status)
		}
	}()
}

func GetPost(id string, outfn func(*ErrorJson, int, *PostJson)) {
	go func() {
		req := xhr.NewRequest("GET", Server+"/api/v1/posts/"+id)
		err := req.Send(nil)
		if err != nil {
			outfn(getError(err.Error()), 0, nil)
			return
		}
		if req.Status >= BAD_REQUEST {
			ej := getError(req.ResponseText)
			outfn(ej, req.Status, nil)
		} else {
			pj, ej := serializePost(req.ResponseText)
			outfn(ej, req.Status, pj)
		}
	}()
}

func GetPosts(pp *PerPage, outfn func(*ErrorJson, int, []PostJson)) {
	go func() {
		var req *xhr.Request
		if pp != nil {
			req = xhr.NewRequest("GET", Server+"/api/v1/posts?"+pp.Params())
		} else {
			req = xhr.NewRequest("GET", Server+"/api/v1/posts")
		}
		err := req.Send(nil)
		if err != nil {
			outfn(getError(err.Error()), 0, nil)
			return
		}
		if req.Status >= BAD_REQUEST {
			ej := getError(req.ResponseText)
			outfn(ej, req.Status, nil)
		} else {
			ps, ej := serializePosts(req.ResponseText)
			outfn(ej, req.Status, ps)
		}
	}()
}

func GetPostsByUser(id string, pp *PerPage, outfn func(*ErrorJson, int, []PostJson)) {
	go func() {
		var req *xhr.Request
		if pp != nil {
			req = xhr.NewRequest("GET", Server+"/api/v1/users/"+id+"/posts?"+pp.Params())
		} else {
			req = xhr.NewRequest("GET", Server+"/api/v1/users/"+id+"/posts")
		}
		err := req.Send(nil)
		if err != nil {
			outfn(getError(err.Error()), 0, nil)
			return
		}
		if req.Status >= BAD_REQUEST {
			ej := getError(req.ResponseText)
			outfn(ej, req.Status, nil)
		} else {
			ps, ej := serializePosts(req.ResponseText)
			outfn(ej, req.Status, ps)
		}
	}()
}
