#!/bin/bash
# we need the script because the gopherjs returns this error
#   vendoring github.com/gopherjs/gopherjs/js package is not supported, see https://github.com/gopherjs/gopherjs/issues/415
go get github.com/PalmStoneGames/polymer
go get honnef.co/go/js/xhr
go get bitbucket.org/rmanolis/cblib
go get honnef.co/go/js/dom
go get github.com/gopherjs/websocket
