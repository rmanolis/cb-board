package pages

import (
	"cb-board/client/actions"
	"cb-board/client/services"
	"cb-board/client/stedux"

	"github.com/PalmStoneGames/polymer"
)

// cb-page-posts
type Posts struct {
	*polymer.Proto
	Posts          []services.PostJson `polymer:"bind"`
	ShowError      bool                `polymer:"bind"`
	ShowDeletePost bool                `polymer:"bind"`
}

func (t *Posts) Ready() {
	t.getPosts()
}

func (t *Posts) Created() {
	stedux.Register(t.actions)
}
func (t *Posts) getPosts() {
	services.GetPosts(nil, func(ej *services.ErrorJson, status int, pjs []services.PostJson) {
		t.Posts = pjs
		t.Notify("posts")
	})
}
func (t *Posts) getNewPost(id string) {
	services.GetPost(id, func(ej *services.ErrorJson, status int, pj *services.PostJson) {
		if ej != nil {
			return
		}
		t.Posts = append([]services.PostJson{*pj}, t.Posts...)
		t.Notify("posts")
	})
}

func (t *Posts) actions(action interface{}) {
	switch ac := action.(type) {
	case *actions.ActionPostDeleted:
		if !ac.HasError {
			for i, v := range t.Posts {
				if v.ID == ac.ID {
					t.Posts = append(t.Posts[:i], t.Posts[i+1:]...)
					t.Notify("posts")
					break
				}
			}
		}
	case *actions.ActionNewPostReceived:
		t.getNewPost(ac.ID)
	}
	stedux.Listeners.Fire()
}
