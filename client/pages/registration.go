package pages

import (
	"cb-board/client/actions"
	"cb-board/client/services"
	"cb-board/client/stedux"

	"github.com/PalmStoneGames/polymer"
)

// cb-page-registration
type Registration struct {
	*polymer.Proto
	Send      chan *polymer.Event `polymer:"handler"`
	PublicKey string              `polymer:"bind"`
	Error     string              `polymer:"bind"`
	ShowError bool                `polymer:"bind"`
}

func (t *Registration) Ready() {
	go func() {
		for {
			select {
			case _ = <-t.Send:
				copts := services.CreateUserOpts{}
				copts.PublicKey = t.PublicKey
				services.CreateUser(&copts, func(ej *services.ErrorJson, status int, uj *services.UserJson) {
					if ej != nil {
						t.Error = ej.Error
						t.ShowError = true
						t.Notify("error", "showError")
						return
					}
					stedux.Dispatch(&actions.ActionChangePath{Path: "/profiles/" + uj.ID})
					t.PublicKey = ""
					t.Notify("publicKey")
				})
			}
		}
	}()
}
