package pages

import (
	"cb-board/client/actions"
	"cb-board/client/stedux"
	"strings"

	"github.com/PalmStoneGames/polymer"
	"honnef.co/go/js/dom"
)

type Route struct {
	Path string
}

type RouteData struct {
	Page string
	Id   string
}

// cb-page-router
type Router struct {
	*polymer.Proto
	Route Route     `polymer:"bind"`
	Data  RouteData `polymer:"bind"` // to get the :page
	Idata RouteData `polymer:"bind"` // to get the :id
}

func (t *Router) Ready() {
	w := dom.GetWindow()
	w.AddEventListener("location-changed", false, func(event dom.Event) {
		event.PreventDefault()
		stedux.Dispatch(&actions.ActionChangePath{Page: t.Data.Page, ID: t.Idata.Id})
	})
	links := strings.Split(dom.GetWindow().Location().Hash, "/")
	if len(links) > 2 {
		stedux.Dispatch(&actions.ActionChangePath{Page: links[1], ID: links[2]})
	}
}

func (t *Router) Created() {
	stedux.Register(t.actions)
}

func (t *Router) actions(action interface{}) {
	switch ac := action.(type) {
	case *actions.ActionChangePath:
		if len(ac.Path) > 0 {
			t.Route.Path = ac.Path
			t.Notify("route.path")
		}
	}
	stedux.Listeners.Fire()
}
