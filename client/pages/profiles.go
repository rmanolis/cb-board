package pages

import (
	"cb-board/client/actions"
	"cb-board/client/services"
	"cb-board/client/stedux"
	"cb-board/client/utils"

	"github.com/PalmStoneGames/polymer"
)

// cb-page-profiles
type Profiles struct {
	*polymer.Proto
	Users         []services.UserJson `polymer:"bind"`
	PublicKey     string              `polymer:"bind"`
	FindPublicKey chan *polymer.Event `polymer:"handler"`
	AllUsers      chan *polymer.Event `polymer:"handler"`
}

func (t *Profiles) Ready() {
	t.getUsers()
	go func() {
		for {
			select {
			case _ = <-t.FindPublicKey:
				sha1, _ := utils.PublicKeyBodyToSha1(t.PublicKey)
				services.GetUserBySha1(sha1, func(ej *services.ErrorJson, status int, uj *services.UserJson) {
					t.Users = []services.UserJson{}
					if ej == nil {
						t.Users = append(t.Users, *uj)
					}
					t.Notify("users")
				})
			case _ = <-t.AllUsers:
				t.getUsers()
			}
		}
	}()
}

func (t *Profiles) Created() {
	stedux.Register(t.actions)
}

func (t *Profiles) actions(action interface{}) {
	switch ac := action.(type) {
	case *actions.ActionChangePath:
		if ac.Page == "profiles" {
			t.getUsers()
		} else {
			t.PublicKey = ""
			t.Notify("publicKey")
		}
	}
	stedux.Listeners.Fire()
}

func (t *Profiles) getUsers() {
	services.GetUsers(nil, func(ej *services.ErrorJson, status int, ujs []services.UserJson) {
		t.Users = ujs
		t.Notify("users")
	})
}
