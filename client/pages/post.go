package pages

import (
	"cb-board/client/actions"
	"cb-board/client/services"
	"cb-board/client/stedux"

	"github.com/PalmStoneGames/polymer"
)

// cb-page-post
type Post struct {
	*polymer.Proto
	Id        string `polymer:"bind"`
	Text      string `polymer:"bind"`
	UserId    string `polymer:"bind"`
	Signature string `polymer:"bind"`
}

func (t *Post) Ready() {
	services.GetPost(t.Id, func(ej *services.ErrorJson, status int, pj *services.PostJson) {
		if ej != nil {
			stedux.Dispatch(&actions.ActionChangePath{Page: ""})
			return
		}
		t.Text = pj.Text
		t.UserId = pj.UserID
		t.Signature = pj.Signature
		t.Notify("text", "userId", "signature")
	})
}

func (t *Post) Created() {
	stedux.Register(t.actions)
}

func (t *Post) actions(action interface{}) {
	switch ac := action.(type) {
	case *actions.ActionChangePath:
		if ac.Page == "post" {
			t.Id = ac.ID
			t.Notify("id")
			t.Ready()
		}
	}
	stedux.Listeners.Fire()
}
