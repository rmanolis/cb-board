package pages

import (
	"cb-board/client/actions"
	"cb-board/client/services"
	"cb-board/client/stedux"

	"github.com/PalmStoneGames/polymer"
)

// cb-page-profile
type Profile struct {
	*polymer.Proto
	Id        string              `polymer:"bind"`
	PublicKey string              `polymer:"bind"`
	Posts     []services.PostJson `polymer:"bind"`
}

func (t *Profile) Ready() {
	println(t.Id)
	services.GetUser(t.Id, func(ej *services.ErrorJson, status int, uj *services.UserJson) {
		if ej != nil {
			stedux.Dispatch(&actions.ActionChangePath{Page: ""})
			return
		}
		println(uj.PublicKey)
		t.PublicKey = uj.PublicKey
		t.Notify("publicKey")
		services.GetPostsByUser(t.Id, nil, func(ej *services.ErrorJson, status int, pjs []services.PostJson) {
			t.Posts = pjs
			t.Notify("posts")
		})
	})
}

func (t *Profile) Created() {
	stedux.Register(t.actions)
}

func (t *Profile) actions(action interface{}) {
	switch ac := action.(type) {
	case *actions.ActionChangePath:
		if ac.Page == "profile" {
			t.Id = ac.ID
			t.Notify("id")
			t.Ready()
		}
	case *actions.ActionPostDeleted:
		if !ac.HasError {
			for i, v := range t.Posts {
				if v.ID == ac.ID {
					t.Posts = append(t.Posts[:i], t.Posts[i+1:]...)
					t.Notify("posts")
					break
				}
			}
		}
	}
	stedux.Listeners.Fire()
}
