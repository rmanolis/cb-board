package models

import (
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const ADMIN_LOGIN_TABLE = "admin_login"

type AdminLogin struct {
	ID        bson.ObjectId `bson:"_id"`
	Username  string        `bson:"username"`
	Text      string        `bson:"text"`
	Signature string        `bson:"signature"`
	CreatedAt time.Time     `bson:"created_at"`
}

type AdminLoginJson struct {
	ID        string    `json:"id"`
	Username  string    `json:"username"`
	Text      string    `json:"text"`
	Signature string    `json:"signature"`
	CreatedAt time.Time `json:"created_at"`
}

func (al *AdminLogin) Json() AdminLoginJson {
	j := AdminLoginJson{}
	j.ID = al.ID.Hex()
	j.Signature = al.Signature
	j.Text = al.Text
	j.Username = al.Username
	j.CreatedAt = al.CreatedAt
	return j
}

func (al *AdminLogin) Insert(db *mgo.Database) error {
	c := db.C(ADMIN_LOGIN_TABLE)
	al.ID = bson.NewObjectId()
	al.CreatedAt = time.Now()
	return c.Insert(&al)
}

func (al *AdminLogin) Update(db *mgo.Database) error {
	c := db.C(ADMIN_LOGIN_TABLE)
	return c.UpdateId(al.ID, &al)
}

func GetAdminLogin(db *mgo.Database, id bson.ObjectId) (*AdminLogin, error) {
	al := new(AdminLogin)
	err := db.C(ADMIN_LOGIN_TABLE).FindId(id).One(&al)
	return al, err
}
