package models

import (
	"cb-board/utils"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const USER_TABLE = "users"

type User struct {
	ID        bson.ObjectId `bson:"_id"`
	PublicKey string        `bson:"public_key"`
	Sha1      string        `bson:"sha1"`
	CreatedAt time.Time     `bson:"created_at"`
}

type UserJson struct {
	ID        string    `json:"id"`
	PublicKey string    `json:"public_key"`
	Sha1      string    `json:"sha1"`
	CreatedAt time.Time `json:"created_at"`
}

func (u *User) Json() UserJson {
	uj := UserJson{}
	uj.ID = u.ID.Hex()
	uj.PublicKey = u.PublicKey
	uj.Sha1 = u.Sha1
	return uj
}

func (u *User) Insert(db *mgo.Database) error {
	c := db.C(USER_TABLE)
	u.ID = bson.NewObjectId()
	return c.Insert(&u)
}

func (u *User) Delete(db *mgo.Database) error {
	c := db.C(USER_TABLE)
	return c.Remove(&u)
}

func GetUserBySha1(db *mgo.Database, sha1 string) (*User, error) {
	c := db.C(USER_TABLE)
	u := new(User)
	err := c.Find(bson.M{
		"sha1": sha1,
	}).One(&u)
	return u, err
}

func GetUserByID(db *mgo.Database, id bson.ObjectId) (*User, error) {
	u := new(User)
	err := db.C(USER_TABLE).FindId(id).One(&u)
	return u, err
}

func GetUsers(db *mgo.Database, opts *utils.PerPage) ([]User, error) {
	c := db.C(USER_TABLE)
	var users = []User{}
	if opts == nil {
		err := c.Find(nil).Sort("-created_at").All(&users)
		return users, err
	}
	skip := opts.Size * opts.Page
	err := c.Find(nil).Skip(skip).Limit(opts.Size).Sort("-created_at").All(&users)
	return users, err

}
