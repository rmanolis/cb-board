package models

import (
	"cb-board/utils"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const POST_TABLE = "posts"

type Post struct {
	ID        bson.ObjectId `bson:"_id"`
	Text      string        `bson:"text"`
	Signature string        `bson:"signature"`
	Sha1      string        `bson:"sha1"`
	UserID    bson.ObjectId `bson:"user_id"`
	CreatedAt time.Time     `bson:"created_at"`
}

type PostJson struct {
	ID        string    `json:"id"`
	Text      string    `json:"text"`
	Signature string    `json:"signature"`
	Sha1      string    `json:"sha1"`
	UserID    string    `json:"user_id"`
	CreatedAt time.Time `json:"created_at"`
}

func (p *Post) Json() PostJson {
	j := PostJson{}
	j.ID = p.ID.Hex()
	j.UserID = p.UserID.Hex()
	j.Signature = p.Signature
	j.Text = p.Text
	j.Sha1 = p.Sha1
	j.CreatedAt = p.CreatedAt
	return j
}

func (p *Post) Insert(db *mgo.Database) error {
	c := db.C(POST_TABLE)
	p.ID = bson.NewObjectId()
	p.CreatedAt = time.Now()
	return c.Insert(&p)
}

func (p *Post) Delete(db *mgo.Database) error {
	c := db.C(POST_TABLE)
	return c.Remove(&p)
}

func GetPostByID(db *mgo.Database, id bson.ObjectId) (*Post, error) {
	p := new(Post)
	err := db.C(POST_TABLE).FindId(id).One(&p)
	return p, err
}

func GetPosts(db *mgo.Database, opts *utils.PerPage) ([]Post, error) {
	c := db.C(POST_TABLE)
	var posts = []Post{}
	if opts == nil {
		err := c.Find(nil).Sort("-created_at").All(&posts)
		return posts, err
	}
	skip := opts.Size * opts.Page
	err := c.Find(nil).Skip(skip).Limit(opts.Size).Sort("-created_at").All(&posts)
	return posts, err

}

func GetPostsByUserID(db *mgo.Database, opts *utils.PerPage, id bson.ObjectId) ([]Post, error) {
	c := db.C(POST_TABLE)
	var posts = []Post{}
	if opts == nil {
		err := c.Find(bson.M{"user_id": id}).Sort("-created_at").All(&posts)
		return posts, err
	}
	skip := opts.Size * opts.Page
	err := c.Find(bson.M{"user_id": id}).Skip(skip).Limit(opts.Size).Sort("-created_at").All(&posts)
	return posts, err
}
