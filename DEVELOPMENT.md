# Tools
go get -u github.com/gopherjs/gopherjs
go get get github.com/cespare/reflex
go get get github.com/codegangsta/gin


# Scripts
client/build-client.sh run this script to build the client and move it to the static folder.
client/reload-client.sh run this script to build the client on every file change.
reload-server.sh run this script to reload the service on any go file.