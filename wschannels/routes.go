package wschannels

import "cb-board/ws"

var Routes = map[string]func(*ws.WSConn, *ws.WSMessage){}
