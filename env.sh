#!/bin/bash
source ../../env.sh
tmux new -s dev -n all -d
tmux new-window -c client -n "client"
tmux new-window -c static -n "static"
tmux new-window -n "mongodb" 'bash mongodb.sh'
tmux attach -t dev