API v1

REQ POST /register
DATA: 
public_key

REQ POST /posts
DATA:
- public_sha256
- post
- digital_signature  (from the post)
VALIDATIONS:
- the post can not be empty
- the digital signature can not be empty
- the public_sha256 can not be empty

REQ PUT /posts/:id/delete
DATA:
- public_sha256
- post
- random_text
- digital_signature  (from the post concatenated with the random_text)
VALIDATIONS:
- the post can not be empty
- the random text can not be less than 100 characters

REQ GET /posts?perPage=num&page=num
RESP DATA:
- List
    - public_sha256
    - post
    - digital_signature

REQ GET /posts/:id
RESP DATA:
- public_sha256
- post
- digital_signature

REQ GET /users/:id/posts?perPage=num&page=num
- List
    - ID
    - public_sha256
    - post
    - digital_signature

REQ GET /users/:id
RESP DATA:
- ID
- public_key
- public_sha256

REQ PUT /users/delete

REQ GET /users?perPage=num&page=num
RESP DATA:
- List
    - ID
    - public_sha256
    - public_key

REQ POST  /ws/new/posts
RESP DATA
- ID 




API Admin

REQ GET /admin/login 
RESP DATA
- ID

REQ POST /admin/login
RESP DATA
- ID
- SIGNATURE
- sha1

REQ GET /admin/is
RESP OK or Unauthorized

REQ DELETE /admin/posts/:id

REQ DELETE /admin/users/:id