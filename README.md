Cryptobureau Board

On the registration of the website the user will create a public-private key and save it in a file.
When the user wants to post a note, the front-end will request the file and ask for the password to create the digital signature.




## Pages

# HOME PAGE
The page will show the list of posts.
Over the posts will be a button with a number over it to show the number of new posts that submitted.
When pressing the number of posts, it will show the new posts as first.
It will have 1 button to delete the post
When "X" pressed, it will open dialog

# REGISTRATION PAGE
The page will two input boxes to add password and repeat it.

# POST PAGE
The page for the specific post.
It will have 1 button to delete the post
When "X" pressed, it will open dialog.

# USERS PAGE
List all users with their public_sha256 and public_keys

# USER PAGE
It contains the list of posts on the left side.
On the right side will contain  public_sha256 and public_keys

# TODO

- dont repost the same text, in configuration file.
- enable hashing for public key based on requests "nonce" from configuration file. To discourage the user from creating many accounts
- different API for admins to delete faster the users and posts
  there will be a grand admin that has his public key in the configuration file.
  then from the API he can add more public keys for other admins
- admins can allow specific people to become users based on a list of passphrases encrypted with the servers private key.
  the admins can print this list in json so they can use it to print in another form and share it to the users.
  the users submit the encrypted passphrase for registration.
  The encrypted passphrase is a json with the passphrase and its signature.
- Add tags.
- The user can comment to other posts
- use cookies to ID users for sending websocket messages 
- add small buttons over the textareas to copy the text from them.

# BUGS
- On the deletion, the error open many modals