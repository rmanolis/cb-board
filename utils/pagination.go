package utils

import (
	"net/http"
	"strconv"
)

type PerPage struct {
	Page int
	Size int
}

func PageSize(r *http.Request) PerPage {
	pp := PerPage{}
	page_str := r.FormValue("page")
	page, err := strconv.Atoi(page_str)
	if err != nil {
		pp.Page = 0
	} else {
		pp.Page = page
	}

	size_str := r.FormValue("size")
	size, err := strconv.Atoi(size_str)
	if err != nil {
		pp.Size = 10
	} else {
		pp.Size = size
	}

	return pp
}
