package utils

import (
	"fmt"
	"log"
	"runtime"
	"strings"
	"time"
)

func init() {
}

func getLine() string {
	_, fileName, fileLine, ok := runtime.Caller(2)
	names := strings.Split(fileName, "cb-board")
	name := names[0]
	if len(names) > 1 {
		name = names[len(names)-1]
	}
	var s string
	if ok {
		s = fmt.Sprintf(time.Now().Format("02/01/06 15:04:05")+" %s:%d-> ", name, fileLine)
	} else {
		s = ""
	}
	return s
}

// Info print info messages in the logs
func Info(v ...interface{}) {
	msg := append([]interface{}{"[INFO] ", getLine()}, v...)
	log.Println(msg...)
}

// Error print error messages in the logs
func Error(v ...interface{}) {
	msg := append([]interface{}{"[ERROR] ", getLine()}, v...)
	log.Println(msg...)
}

// Debug prints in the log the debug messages
func Debug(v ...interface{}) {
	msg := append([]interface{}{"[DEBUG] ", getLine()}, v...)
	log.Println(msg...)
}

// Fatal prints the last message and followed by os.Exit(1)
func Fatal(v ...interface{}) {
	msg := append([]interface{}{"[FATAL] ", getLine()}, v...)
	log.Fatalln(msg...)
}
