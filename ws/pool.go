package ws

import (
	"cb-board/utils"
	"sync"
)

// SessionPool is a structure for thread-safely storing sessions and broadcasting messages to them.
type SessionPool struct {
	mu          sync.RWMutex
	connections map[string]WSConn
}

func NewSessionPool() (p *SessionPool) {
	p = new(SessionPool)
	p.connections = make(map[string]WSConn)
	return p
}

// Add adds the given session to the session pool.
func (p *SessionPool) Add(c *WSConn) {
	p.mu.Lock()
	defer p.mu.Unlock()
	p.connections[c.Id] = *c
}

// Remove removes the given session from the session pool.
// It is safe to remove non-existing sessions.
func (p *SessionPool) Remove(c *WSConn) {
	p.mu.Lock()
	defer p.mu.Unlock()
	_, ok := p.connections[c.Id]
	if ok {
		delete(p.connections, c.Id)
	}
}

// Broadcast sends the given message to every session in the pool.
func (p *SessionPool) Broadcast(m []byte) {
	defer func() {
		if r := recover(); r != nil {
			_, ok := r.(error)
			if !ok {
				utils.Error("Websocket error: %v \n", r)
			}
		}
	}()

	p.mu.RLock()
	defer p.mu.RUnlock()
	for _, c := range p.connections {
		select {
		case c.Send <- m:
		default:
			p.Remove(&c)
			close(c.Send)
		}
	}
}
