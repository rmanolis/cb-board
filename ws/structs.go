package ws

import (
	"cb-board/utils"
	"encoding/json"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

// connection is an middleman between the websocket connection and the hub.
type WSConn struct {
	mu sync.RWMutex
	// The websocket connection.
	WS *websocket.Conn
	// Buffered channel of outbound messages.
	Send           chan []byte
	CloseProcesses chan bool
	closed         bool
	Id             string
	UserID         string
	ExpiresAt      time.Time
}

type WSMessage struct {
	Channel string `json:"channel"`
	Message string `json:"message"`
}

func (ws *WSConn) ToChannel(channel string, message []byte) {
	if ws.IsClosed() {
		utils.Error("Websocket with id " + ws.Id + " for user id " + ws.UserID + " for channel " + channel + " is closed ")
		return
	}
	defer func() {
		if r := recover(); r != nil {
			_, ok := r.(error)
			if !ok {
				utils.Error("Websocket error: ", r)
			}
		}
	}()
	wsm := WSMessage{
		Channel: channel,
		Message: string(message),
	}
	out, err := json.Marshal(wsm)
	if err != nil {
		utils.Error("error with jsoning wsmessage")
		return
	}
	ws.Send <- out
}

func (ws *WSConn) Close() {
	ws.mu.Lock()
	defer ws.mu.Unlock()
	ws.closed = true
}

func (ws *WSConn) IsClosed() bool {
	ws.mu.Lock()
	defer ws.mu.Unlock()
	return ws.closed
}
