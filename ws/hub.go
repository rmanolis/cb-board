package ws

import (
	"cb-board/utils"
	"encoding/json"
)

// hub maintains the set of active connections and broadcasts messages to the
// connections.
type HubPool struct {
	// Registered connections.
	pool *SessionPool

	// Inbound messages from the connections.
	broadcast chan []byte

	onchannel chan map[*WSConn][]byte

	// Register requests from the connections.
	register chan *WSConn

	// Unregister requests from connections.
	unregister chan *WSConn
}

func NewHub() *HubPool {
	utils.Info("Startin a new hub pool")
	h := HubPool{
		broadcast:  make(chan []byte),
		onchannel:  make(chan map[*WSConn][]byte),
		register:   make(chan *WSConn),
		unregister: make(chan *WSConn),
		pool:       NewSessionPool(),
	}
	return &h
}

var Hub = NewHub()
var routes = map[string]func(*WSConn, *WSMessage){}

func HubRun(rs map[string]func(*WSConn, *WSMessage)) {
	routes = rs
	Hub.Run()
}

func (h *HubPool) Run() {
	utils.Info("Hub pool is running")
	for {
		select {
		case c := <-h.register:
			utils.Info("Reginstering ws user ", c.Id)
			Hub.pool.Add(c)
			utils.Info("Registered")

		case c := <-h.unregister:
			utils.Info("Unregistering ", c.Id)
			h.pool.Remove(c)
			close(c.Send)
			c.Close()

		case m := <-h.broadcast:
			h.pool.Broadcast(m)

		case m := <-h.onchannel:
			for k, v := range m {
				go routeChannels(k, v)
			}
		}
	}
}

func routeChannels(con *WSConn, m []byte) {
	wsm := new(WSMessage)
	err := json.Unmarshal(m, wsm)
	if err != nil {
		utils.Error("Could not unmarshal the wsmessage ", err)
	}
	f, ok := routes[wsm.Channel]
	if ok {
		f(con, wsm)
	} else {
		utils.Error("Did not find the channel ", wsm.Channel)
	}
}

func Broadcast(channel string, message []byte) {
	wsm := WSMessage{
		Channel: channel,
		Message: string(message),
	}
	out, err := json.Marshal(wsm)
	if err != nil {
		utils.Error(err)
	}

	Hub.broadcast <- out
}

func Register(c *WSConn) {
	Hub.register <- c
	wsm := WSMessage{
		Channel: "register",
		Message: c.Id,
	}
	f, ok := routes[wsm.Channel]
	if ok {
		f(c, &wsm)
	}

}

func Unregister(c *WSConn) {
	wsm := WSMessage{
		Channel: "unregister",
		Message: c.Id,
	}
	f, ok := routes[wsm.Channel]
	if ok {
		f(c, &wsm)
	}

	Hub.unregister <- c
}

func OnChannel(c *WSConn, m []byte) {
	v := map[*WSConn][]byte{
		c: m,
	}
	Hub.onchannel <- v
}

func ToSession(channel, session string, m []byte) {
	wc, ok := Hub.pool.connections[session]
	if !ok {
		utils.Error("pool does not have session ", session)
		return
	}
	wc.ToChannel(channel, m)
}

func ToAll(channel string, m []byte) {
	for _, wc := range Hub.pool.connections {
		wc.ToChannel(channel, m)
	}
}
