package ctrls

import (
	"cb-board/models"
	"cb-board/utils"
	"errors"
	"net/http"

	"bitbucket.org/rmanolis/cblib"
	"gopkg.in/mgo.v2/bson"

	mgo "gopkg.in/mgo.v2"
)

type userCtrls struct {
	db *mgo.Database
}

func NewUserCtrls(db *mgo.Database) *userCtrls {
	rc := new(userCtrls)
	rc.db = db
	return rc
}

func (uc *userCtrls) GetUsers(opts *utils.PerPage) ([]models.UserJson, int, error) {
	users, _ := models.GetUsers(uc.db, opts)
	ujs := []models.UserJson{}
	for _, v := range users {
		ujs = append(ujs, v.Json())
	}
	return ujs, http.StatusOK, nil
}

func (uc *userCtrls) GetUser(id string) (*models.UserJson, int, error) {
	if !bson.IsObjectIdHex(id) {
		return nil, http.StatusUnprocessableEntity, errors.New("id is not in the correct format")
	}
	user, err := models.GetUserByID(uc.db, bson.ObjectIdHex(id))
	if err != nil {
		return nil, http.StatusNotFound, errors.New("user is not found")
	}
	uj := user.Json()
	return &uj, http.StatusOK, nil
}

type DeleteUserOpts struct {
	UserID       string `json:"user_id"`
	PublicKey    string `json:"public_key"`
	RandomString string `json:"random_string"`
	Signature    string `json:"signature"`
}

func (uc *userCtrls) validateDeleteUser(opts *DeleteUserOpts) (*models.User, int, error) {
	if len(opts.RandomString) < 20 {
		return nil, http.StatusUnprocessableEntity, errors.New("random string is lower than 20 characters")
	}
	if !bson.IsObjectIdHex(opts.UserID) {
		return nil, http.StatusUnprocessableEntity, errors.New("user id is not in the correct format")
	}
	user, err := models.GetUserByID(uc.db, bson.ObjectIdHex(opts.UserID))
	if err != nil {
		return nil, http.StatusUnprocessableEntity, errors.New("user does not exists")
	}
	if user.PublicKey != opts.PublicKey {
		return nil, http.StatusUnprocessableEntity, errors.New("the public key is not user's public key")
	}

	is, err := cblib.Verify([]byte(user.PublicKey), []byte(opts.PublicKey+opts.RandomString), []byte(opts.Signature))
	if err != nil {
		return nil, http.StatusUnprocessableEntity, err
	}
	if !is {
		return nil, http.StatusUnprocessableEntity, errors.New("the signature does not verify the text")
	}
	return user, http.StatusNoContent, nil
}

func (uc *userCtrls) DeleteUser(opts *DeleteUserOpts) (int, error) {
	user, status, err := uc.validateDeleteUser(opts)
	if err != nil {
		return status, err
	}
	err = user.Delete(uc.db)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	return http.StatusNoContent, nil
}

func (uc *userCtrls) GetUserBySha1(sha1 string) (*models.UserJson, int, error) {
	user, err := models.GetUserBySha1(uc.db, sha1)
	if err != nil {
		return nil, http.StatusNotFound, errors.New("user is not found")
	}
	uj := user.Json()
	return &uj, http.StatusOK, nil
}
