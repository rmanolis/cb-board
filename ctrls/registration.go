package ctrls

import (
	"cb-board/models"
	"cb-board/utils"
	"errors"
	"net/http"

	"bitbucket.org/rmanolis/cblib"
	"gopkg.in/mgo.v2"
)

type registerCtrl struct {
	db *mgo.Database
}

func NewRegisterCtrls(db *mgo.Database) *registerCtrl {
	rc := new(registerCtrl)
	rc.db = db
	return rc
}

type RegisterOpts struct {
	PublicKey string `json:"public_key"`
}

func (rc *registerCtrl) validateRegistration(opts *RegisterOpts) (string, int, error) {
	if len(opts.PublicKey) == 0 {
		return "", http.StatusUnprocessableEntity, errors.New("public key is empty")
	}
	pk := cblib.PublicKey{}
	err := pk.UnmarshalText([]byte(opts.PublicKey))
	if err != nil {
		return "", http.StatusUnprocessableEntity, err
	}
	sha1, err := utils.PublicKeyBodyToSha1(opts.PublicKey)
	if err != nil {
		return "", http.StatusUnprocessableEntity, err
	}
	_, err = models.GetUserBySha1(rc.db, sha1)
	if err == nil {
		return "", http.StatusUnprocessableEntity, errors.New("the public key already exists")
	}
	return sha1, http.StatusOK, nil
}

func (rc *registerCtrl) RegisterKey(opts *RegisterOpts) (*models.UserJson, int, error) {
	sha1, status, err := rc.validateRegistration(opts)
	if err != nil {
		return nil, status, err
	}
	u := models.User{}
	u.PublicKey = opts.PublicKey
	u.Sha1 = sha1
	err = u.Insert(rc.db)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	uj := u.Json()
	return &uj, http.StatusOK, nil
}
