package ctrls

import (
	"cb-board/utils"
	"crypto/rand"
	"net/http"
	"testing"

	"bitbucket.org/rmanolis/cblib"
	"github.com/stretchr/testify/assert"
)

func TestGetUsersTwoPerPage(t *testing.T) {
	db := startDB()
	defer endDB(db)
	NewUserTest(t, db)
	NewUserTest(t, db)
	NewUserTest(t, db)
	NewUserTest(t, db)
	uc := NewUserCtrls(db)
	pp := utils.PerPage{}
	pp.Page = 0
	pp.Size = 2
	users, status, err := uc.GetUsers(&pp)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, 2, len(users))
	assert.Nil(t, err)

	pp.Page = 1
	users, status, err = uc.GetUsers(&pp)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, 2, len(users))
	assert.Nil(t, err)
}

func TestGetUser(t *testing.T) {
	db := startDB()
	defer endDB(db)
	u, _ := NewUserTest(t, db)
	uc := NewUserCtrls(db)
	uj, status, err := uc.GetUser(u.ID)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, u.ID, uj.ID)
	assert.Nil(t, err)
}

func TestDeleteUser(t *testing.T) {
	db := startDB()
	defer endDB(db)
	u, k := NewUserTest(t, db)
	uc := NewUserCtrls(db)
	duo := DeleteUserOpts{}
	duo.UserID = u.ID
	duo.PublicKey = u.PublicKey
	duo.RandomString = utils.RandomString(20)
	sig, err := cblib.Sign([]byte(duo.PublicKey+duo.RandomString), k.PrivateKey, rand.Reader)
	assert.Nil(t, err)
	duo.Signature = string(sig)
	status, err := uc.DeleteUser(&duo)
	assert.Equal(t, http.StatusNoContent, status)
	assert.Nil(t, err)
}

func TestGetUserBySha1(t *testing.T) {
	db := startDB()
	defer endDB(db)
	u, _ := NewUserTest(t, db)
	sha1, _ := utils.PublicKeyBodyToSha1(u.PublicKey)
	uc := NewUserCtrls(db)
	u1, status, err := uc.GetUserBySha1(sha1)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, u.ID, u1.ID)
	assert.Nil(t, err)
}
