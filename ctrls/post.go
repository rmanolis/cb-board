package ctrls

import (
	"cb-board/confs"
	"cb-board/models"
	"cb-board/utils"
	"errors"
	"fmt"
	"net/http"

	"cb-board/ws"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/rmanolis/cblib"

	mgo "gopkg.in/mgo.v2"
)

type postCtrls struct {
	db *mgo.Database
}

func NewPostCtrls(db *mgo.Database) *postCtrls {
	rc := new(postCtrls)
	rc.db = db
	return rc
}

type CreatePostOpts struct {
	Text      string `json:"text"`
	Signature string `json:"signature"`
	Sha1      string `json:"sha1"`
}

func (pc *postCtrls) validateCreatePost(opts *CreatePostOpts) (*models.User, int, error) {
	if len(opts.Text) == 0 {
		return nil, http.StatusUnprocessableEntity, errors.New("text is empty")
	}
	if len(opts.Signature) == 0 {
		return nil, http.StatusUnprocessableEntity, errors.New("signature is empty")
	}
	if len(opts.Sha1) == 0 {
		return nil, http.StatusUnprocessableEntity, errors.New("sha1, from the public key, is empty")
	}

	if len(opts.Text) > confs.Conf.LimitCharacters && confs.Conf.LimitCharacters > -1 {
		return nil, http.StatusUnprocessableEntity, errors.New(fmt.Sprint("the post is over the limit of characters which is ", confs.Conf.LimitCharacters))
	}
	user, err := models.GetUserBySha1(pc.db, opts.Sha1)
	if err != nil {
		return nil, http.StatusUnprocessableEntity, errors.New("we don't have the public key in the database")
	}
	is, err := cblib.Verify([]byte(user.PublicKey), []byte(opts.Text), []byte(opts.Signature))
	if err != nil {
		return nil, http.StatusUnprocessableEntity, err
	}
	if !is {
		return nil, http.StatusUnprocessableEntity, errors.New("the signature does not verify the text")
	}
	return user, http.StatusOK, nil
}

func (pc *postCtrls) CreatePost(opts *CreatePostOpts) (*models.PostJson, int, error) {
	user, status, err := pc.validateCreatePost(opts)
	if err != nil {
		return nil, status, err
	}
	post := models.Post{}
	post.Signature = opts.Signature
	post.Text = opts.Text
	post.Sha1 = opts.Sha1
	post.UserID = user.ID
	err = post.Insert(pc.db)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	pj := post.Json()
	ws.ToAll("new-post", []byte(pj.ID))
	return &pj, http.StatusOK, nil
}

type DeletePostOpts struct {
	ID           string `json:"id"`
	RandomString string `json:"random_string"`
	Signature    string `json:"signature"`
}

func (pc *postCtrls) validateDeletePost(opts *DeletePostOpts) (*models.Post, int, error) {
	if len(opts.ID) == 0 {
		return nil, http.StatusUnprocessableEntity, errors.New("id is empty")
	}
	if len(opts.RandomString) < 20 {
		return nil, http.StatusUnprocessableEntity, errors.New("random string is lower than 20 characters")
	}
	if !bson.IsObjectIdHex(opts.ID) {
		return nil, http.StatusUnprocessableEntity, errors.New("id is not in the correct format")
	}
	post, err := models.GetPostByID(pc.db, bson.ObjectIdHex(opts.ID))
	if err != nil {
		return nil, http.StatusUnprocessableEntity, errors.New("post does not exists")
	}
	user, err := models.GetUserByID(pc.db, post.UserID)
	if err != nil {
		utils.Error(err)
		return nil, http.StatusInternalServerError, errors.New("post does not have a user")
	}
	is, err := cblib.Verify([]byte(user.PublicKey), []byte(opts.ID+opts.RandomString), []byte(opts.Signature))
	if err != nil {
		return nil, http.StatusUnprocessableEntity, err
	}
	if !is {
		return nil, http.StatusUnprocessableEntity, errors.New("the signature does not verify the text")
	}
	return post, http.StatusNoContent, nil
}

func (pc *postCtrls) DeletePost(opts *DeletePostOpts) (int, error) {
	post, status, err := pc.validateDeletePost(opts)
	if err != nil {
		return status, err
	}
	err = post.Delete(pc.db)
	if err != nil {
		return http.StatusInternalServerError, err
	}
	ws.ToAll("deleted-post", []byte(post.ID.Hex()))
	return http.StatusNoContent, nil
}

func (pc *postCtrls) GetPostByID(id string) (*models.PostJson, int, error) {
	if !bson.IsObjectIdHex(id) {
		return nil, http.StatusUnprocessableEntity, errors.New("id is not in the correct format")
	}
	post, err := models.GetPostByID(pc.db, bson.ObjectIdHex(id))
	if err != nil {
		return nil, http.StatusNotFound, errors.New("post does not exists")
	}

	pj := post.Json()
	return &pj, http.StatusOK, nil
}

func (pc *postCtrls) GetPosts(opts *utils.PerPage) ([]models.PostJson, int, error) {
	posts, _ := models.GetPosts(pc.db, opts)
	pjs := []models.PostJson{}
	for _, v := range posts {
		pjs = append(pjs, v.Json())
	}
	return pjs, http.StatusOK, nil
}

func (pc *postCtrls) GetPostsByUser(opts *utils.PerPage, id string) ([]models.PostJson, int, error) {
	if !bson.IsObjectIdHex(id) {
		return nil, http.StatusUnprocessableEntity, errors.New("id is not in the correct format")
	}
	_, err := models.GetUserByID(pc.db, bson.ObjectIdHex(id))
	if err != nil {
		return nil, http.StatusNotFound, errors.New("user is not found")
	}
	posts, _ := models.GetPostsByUserID(pc.db, opts, bson.ObjectIdHex(id))
	pjs := []models.PostJson{}
	for _, v := range posts {
		pjs = append(pjs, v.Json())
	}
	return pjs, http.StatusOK, nil
}
