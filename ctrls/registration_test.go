package ctrls

import (
	"cb-board/models"
	"cb-board/utils"
	"crypto/rand"
	"net/http"
	"testing"

	"bitbucket.org/rmanolis/cblib"
	"github.com/stretchr/testify/assert"
	mgo "gopkg.in/mgo.v2"
)

func startDB() *mgo.Database {
	ses, err := mgo.Dial("localhost")
	if err != nil {
		utils.Fatal("cannot dial mongo", err)
	}
	db := ses.DB("test")
	return db
}

func endDB(db *mgo.Database) {
	err := db.DropDatabase()
	if err != nil {
		utils.Fatal("cannot delete db", err)
	}
}

func TestRegisterAcceptance(t *testing.T) {
	db := startDB()
	defer endDB(db)
	k, err := cblib.GenerateKeys(cblib.P256, rand.Reader)
	assert.Nil(t, err)
	ro := RegisterOpts{
		PublicKey: string(k.PublicKey),
	}
	reg := NewRegisterCtrls(db)
	user, status, err := reg.RegisterKey(&ro)
	assert.Equal(t, http.StatusOK, status)
	assert.Nil(t, err)
	assert.NotNil(t, user)
}

func NewUserTest(t *testing.T, db *mgo.Database) (*models.UserJson, *cblib.Keys) {
	k, err := cblib.GenerateKeys(cblib.P256, rand.Reader)
	assert.Nil(t, err)
	ro := RegisterOpts{
		PublicKey: string(k.PublicKey),
	}
	reg := NewRegisterCtrls(db)
	user, status, err := reg.RegisterKey(&ro)
	assert.Equal(t, http.StatusOK, status)
	assert.Nil(t, err)
	return user, k
}

func TestRegisterPublicKeyExists(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, _ := NewUserTest(t, db)
	ro := RegisterOpts{
		PublicKey: uj.PublicKey,
	}
	reg := NewRegisterCtrls(db)
	user, status, err := reg.RegisterKey(&ro)
	assert.NotNil(t, err)
	assert.Nil(t, user)
	assert.Equal(t, http.StatusUnprocessableEntity, status)
	ro.PublicKey = "1111\n" + ro.PublicKey
	//adding characters to the public key and check again
	ro.PublicKey += "lalsdlasldlalsdlalsdlalsdlalsdlalsd"
	user, status, err = reg.RegisterKey(&ro)
	assert.NotNil(t, err)
	t.Log(err)
	assert.Nil(t, user)
	assert.Equal(t, http.StatusUnprocessableEntity, status)
}

func TestRegisterPublicKeyIsMissingCharacters(t *testing.T) {
	db := startDB()
	defer endDB(db)
	// removed 'Q==' at the end
	pk := `-----BEGIN CB PUBLIC KEY-----
	curve: P256
	
	MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEpYIZWQlClcP4uREiXaHG5W+hQUvv
	DqAYTQWVHmzPEprDR/3cAbTUaytKGNA0POHuS6NsPwYsQ+Q8Ydk7lJ3hN
	-----END CB PUBLIC KEY-----`
	ro := RegisterOpts{
		PublicKey: pk,
	}
	reg := NewRegisterCtrls(db)
	user, status, err := reg.RegisterKey(&ro)
	t.Log(err)
	assert.NotNil(t, err)
	assert.Nil(t, user)
	assert.Equal(t, http.StatusUnprocessableEntity, status)
}

func TestRegisterPublicKeyMissingCurve(t *testing.T) {
	db := startDB()
	defer endDB(db)
	pk := `-----BEGIN CB PUBLIC KEY-----


MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEpYIZWQlClcP4uREiXaHG5W+hQUvv
DqAYTQWVHmzPEprDR/3cAbTUaytKGNA0POHuS6NsPwYsQ+Q8Ydk7lJ3hNQ==
-----END CB PUBLIC KEY-----`
	ro := RegisterOpts{
		PublicKey: pk,
	}
	reg := NewRegisterCtrls(db)
	user, status, err := reg.RegisterKey(&ro)
	t.Log(err)
	assert.NotNil(t, err)
	assert.Nil(t, user)
	assert.Equal(t, http.StatusUnprocessableEntity, status)
}
