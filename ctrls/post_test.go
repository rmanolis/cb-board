package ctrls

import (
	"cb-board/confs"
	"cb-board/models"
	"cb-board/utils"
	"crypto/rand"
	"net/http"
	"testing"

	"gopkg.in/mgo.v2/bson"

	"gopkg.in/mgo.v2"

	"bitbucket.org/rmanolis/cblib"
	"github.com/stretchr/testify/assert"
)

func TestCreatePostSuccess(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	opts := CreatePostOpts{}
	opts.Text = "demo text"
	sig, err := cblib.Sign([]byte(opts.Text), k.PrivateKey, rand.Reader)
	assert.Nil(t, err)
	opts.Signature = string(sig)
	opts.Sha1 = uj.Sha1
	uc := NewPostCtrls(db)
	post, status, err := uc.CreatePost(&opts)
	assert.Equal(t, http.StatusOK, status)
	assert.Nil(t, err)
	assert.NotNil(t, post)
	t.Log(post)
}

func TestCreatePostDoesNotValidateText(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	opts := CreatePostOpts{}
	opts.Text = "demo text"
	sig, err := cblib.Sign([]byte(opts.Text), k.PrivateKey, rand.Reader)
	// change opts text to fail on validation
	opts.Text += " "
	assert.Nil(t, err)
	opts.Signature = string(sig)
	opts.Sha1 = uj.Sha1
	uc := NewPostCtrls(db)
	post, status, err := uc.CreatePost(&opts)
	assert.Equal(t, http.StatusUnprocessableEntity, status)
	assert.NotNil(t, err)
	assert.Nil(t, post)
}

func TestCreatePostSha1DoesNotExists(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	opts := CreatePostOpts{}
	opts.Text = "demo text"
	sig, err := cblib.Sign([]byte(opts.Text), k.PrivateKey, rand.Reader)
	assert.Nil(t, err)
	opts.Signature = string(sig)
	// faking sha1 so it does not exists.
	opts.Sha1 = uj.Sha1 + "1"
	uc := NewPostCtrls(db)
	post, status, err := uc.CreatePost(&opts)
	assert.Equal(t, http.StatusUnprocessableEntity, status)
	assert.NotNil(t, err)
	assert.Nil(t, post)
}

func TestCreatePostOverTheLimitOfCharacters(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	oldLimit := confs.Conf.LimitCharacters
	confs.Conf.LimitCharacters = 3
	opts := CreatePostOpts{}
	opts.Text = "demo text"
	sig, err := cblib.Sign([]byte(opts.Text), k.PrivateKey, rand.Reader)
	assert.Nil(t, err)
	opts.Signature = string(sig)
	opts.Sha1 = uj.Sha1
	uc := NewPostCtrls(db)
	post, status, err := uc.CreatePost(&opts)
	assert.Equal(t, http.StatusUnprocessableEntity, status)
	assert.NotNil(t, err)
	t.Log(err)
	assert.Nil(t, post)
	// avoiding bugs because of the global object
	confs.Conf.LimitCharacters = oldLimit
}

func NewPostTest(t *testing.T, db *mgo.Database, k *cblib.Keys, uj *models.UserJson) *models.PostJson {
	opts := CreatePostOpts{}
	opts.Text = "demo text"
	sig, err := cblib.Sign([]byte(opts.Text), k.PrivateKey, rand.Reader)
	assert.Nil(t, err)
	opts.Signature = string(sig)
	opts.Sha1 = uj.Sha1
	uc := NewPostCtrls(db)
	post, status, err := uc.CreatePost(&opts)
	assert.Equal(t, http.StatusOK, status)
	assert.Nil(t, err)
	assert.NotNil(t, post)
	return post
}

func TestDeletePostSuccess(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	post := NewPostTest(t, db, k, uj)
	opts := DeletePostOpts{}
	opts.ID = post.ID
	opts.RandomString = utils.RandomString(20)
	sig, err := cblib.Sign([]byte(opts.ID+opts.RandomString), k.PrivateKey, rand.Reader)
	assert.Nil(t, err)
	opts.Signature = string(sig)
	uc := NewPostCtrls(db)
	status, err := uc.DeletePost(&opts)
	assert.Equal(t, http.StatusNoContent, status)
	assert.Nil(t, err)
	_, err = models.GetPostByID(db, bson.ObjectIdHex(opts.ID))
	assert.NotNil(t, err)
}

func TestDeletePostIdDoesNotExists(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	post := NewPostTest(t, db, k, uj)
	dbpost, err := models.GetPostByID(db, bson.ObjectIdHex(post.ID))
	assert.Nil(t, err)
	// deleting post, so it does not find it later
	err = dbpost.Delete(db)
	assert.Nil(t, err)
	opts := DeletePostOpts{}
	opts.ID = post.ID
	opts.RandomString = utils.RandomString(20)
	sig, err := cblib.Sign([]byte(opts.ID+opts.RandomString), k.PrivateKey, rand.Reader)
	assert.Nil(t, err)
	opts.Signature = string(sig)
	uc := NewPostCtrls(db)
	status, err := uc.DeletePost(&opts)
	assert.Equal(t, http.StatusUnprocessableEntity, status)
	assert.NotNil(t, err)
	t.Log(err)
	_, err = models.GetPostByID(db, bson.ObjectIdHex(opts.ID))
	assert.NotNil(t, err)
}

func TestDeletePostVerificationFailed(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	post := NewPostTest(t, db, k, uj)
	opts := DeletePostOpts{}
	opts.ID = post.ID
	opts.RandomString = utils.RandomString(20)
	sig, err := cblib.Sign([]byte(opts.ID+opts.RandomString), k.PrivateKey, rand.Reader)
	// adding an extra character to fail the verification
	opts.RandomString += " "
	assert.Nil(t, err)
	opts.Signature = string(sig)
	uc := NewPostCtrls(db)
	status, err := uc.DeletePost(&opts)
	assert.Equal(t, http.StatusUnprocessableEntity, status)
	assert.NotNil(t, err)
	t.Log(err)
}

func TestGetPostSuccess(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	post := NewPostTest(t, db, k, uj)
	uc := NewPostCtrls(db)
	post, status, err := uc.GetPostByID(post.ID)
	assert.Equal(t, http.StatusOK, status)
	assert.Nil(t, err)
	assert.NotNil(t, post)
}

func TestGetPostNotFound(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	post := NewPostTest(t, db, k, uj)
	// deleting post, so it does not find it later
	dbpost, err := models.GetPostByID(db, bson.ObjectIdHex(post.ID))
	assert.Nil(t, err)
	err = dbpost.Delete(db)
	assert.Nil(t, err)
	uc := NewPostCtrls(db)
	post, status, err := uc.GetPostByID(post.ID)
	assert.Equal(t, http.StatusNotFound, status)
	assert.NotNil(t, err)
	assert.Nil(t, post)
}

func TestGetPostsSuccess(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	NewPostTest(t, db, k, uj)
	NewPostTest(t, db, k, uj)
	uc := NewPostCtrls(db)
	posts, status, err := uc.GetPosts(nil)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, 2, len(posts))
	assert.Nil(t, err)
}

func TestGetPostsTwoPerPage(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	NewPostTest(t, db, k, uj)
	NewPostTest(t, db, k, uj)
	NewPostTest(t, db, k, uj)
	NewPostTest(t, db, k, uj)
	uc := NewPostCtrls(db)
	pp := utils.PerPage{}
	pp.Page = 0
	pp.Size = 2
	posts, status, err := uc.GetPosts(&pp)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, 2, len(posts))
	assert.Nil(t, err)

	pp.Page = 1
	posts, status, err = uc.GetPosts(&pp)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, 2, len(posts))
	assert.Nil(t, err)
}

func TestGetPostsByUserTwoPerPage(t *testing.T) {
	db := startDB()
	defer endDB(db)
	uj, k := NewUserTest(t, db)
	NewPostTest(t, db, k, uj)
	NewPostTest(t, db, k, uj)
	NewPostTest(t, db, k, uj)
	NewPostTest(t, db, k, uj)
	uc := NewPostCtrls(db)
	pp := utils.PerPage{}
	pp.Page = 0
	pp.Size = 2
	posts, status, err := uc.GetPostsByUser(&pp, uj.ID)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, 2, len(posts))
	assert.Nil(t, err)

	pp.Page = 1
	posts, status, err = uc.GetPostsByUser(&pp, uj.ID)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, 2, len(posts))
	assert.Nil(t, err)
}
