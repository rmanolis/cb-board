package confs

import (
	"cb-board/utils"
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	LimitCharacters    int
	DeletingEveryHours int
	AdminPublicKey     map[string]string
}

var Conf *Config

func init() {
	out, err := ioutil.ReadFile("config.json")
	if err != nil {
		utils.Error(err)
	} else {
		c := Config{}
		err := json.Unmarshal(out, &c)
		if err != nil {
			utils.Error(err)

		} else {
			Conf = &c
		}
	}
	if Conf == nil {
		Conf = new(Config)
		Conf.LimitCharacters = -1
		Conf.DeletingEveryHours = -1
	}
}
